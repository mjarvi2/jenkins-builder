# Jenkins Builder
---

## Building the master
---

1. Create an ubuntu system
2. Install git
3. Clone this repo
4. run install.sh (the credentials don't need to be legit)
5. visit jenkins at http://your-host:8080

## Build the agents
---
1. This is an incomplete implementation. I currently use a build job within my seed repository to build the templates, deploy boxes, and create the agents.
2. generate-agent.py - This gets run to create the configs on the jenkins master for an agent which calls back. Writes the secret the agent needs to a file.
3. install.sh and Dockerfile - These are for actually installing the agent. You will need the token from the generate-agent.py output.
