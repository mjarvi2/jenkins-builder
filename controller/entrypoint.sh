#!/bin/bash
set -e

set -- tini -- /usr/local/bin/jenkins.sh "$@"

chmod 777 /var/run/docker.sock

exec "$@"
