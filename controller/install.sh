#!/bin/bash

prompt_for_input() {
    local prompt_message="$1"
    local variable_name="$2"

    read -p "$prompt_message" $variable_name
}

while getopts ":p:u:k:i:a:t:w:s:" opt; do
  case $opt in
    p) admin_password="$OPTARG" ;;
    u) proxmox_username="$OPTARG" ;;
    k) proxmox_password="$OPTARG" ;;
    i) proxmox_api_id="$OPTARG" ;;
    a) proxmox_api_key="$OPTARG" ;;
    t) discord_token="$OPTARG" ;;
    w) discord_webhook="$OPTARG" ;;
    s) seed_repo="$OPTARG" ;;
    \?) echo "Invalid option: -$OPTARG" >&2 ;;
    :) echo "Option -$OPTARG requires an argument." >&2 ;;
  esac
done

if [ -z "$admin_password" ]; then
    prompt_for_input "Enter the admin password for Jenkins: " admin_password
fi

if [ -z "$proxmox_username" ]; then
    prompt_for_input "Enter the user to use for proxmox (i.e. root): " proxmox_username
fi

if [ -z "$proxmox_password" ]; then
    prompt_for_input "Enter the password to use for proxmox: " proxmox_password
fi

if [ -z "$proxmox_api_id" ]; then
    prompt_for_input "Enter the proxmox API credential ID (i.e. 'root@pam!terraform'): " proxmox_api_id
fi

if [ -z "$proxmox_api_key" ]; then
    prompt_for_input "Enter the proxmox API key: " proxmox_api_key
fi

if [ -z "$discord_token" ]; then
    prompt_for_input "Enter the discord bot token: " discord_token
fi

if [ -z "$discord_webhook" ]; then
    prompt_for_input "Enter the discord webhook: " discord_webhook
fi

if [ -z "$seed_repo" ]; then
    prompt_for_input "Enter the seed repo: " seed_repo
fi

# Make all .sh files executable
chmod +x *.sh

./install-docker.sh

PRIVATE_KEY_PATH="key"

# Check if the private key already exists
if [ ! -f "$PRIVATE_KEY_PATH" ]; then
  ssh-keygen -t rsa -f "$PRIVATE_KEY_PATH" -P "" &> /dev/null
fi

# Read the SSH private key contents
PRIVATE_KEY_CONTENTS=$(< "$PRIVATE_KEY_PATH")

# Escape the private key contents for YAML compatibility
ESCAPED_PRIVATE_KEY_CONTENTS=$(echo "$PRIVATE_KEY_CONTENTS" | sed 's/^/                    /')

# Safely replace the placeholder in the Jenkins template with the actual private key contents
awk -v key="$ESCAPED_PRIVATE_KEY_CONTENTS" '{
    if ($0 ~ /YOUR_PRIVATE_KEY_CONTENTS/) {
        print key
    } else {
        print
    }
}' jenkins-template.yaml > jenkins.yaml

# Update the Jenkins YAML file with the provided inputs
sed -i "s/PLACEHOLDER_PASSWORD/$admin_password/" jenkins.yaml
sed -i "s/PLACEHOLDER_PROXMOX_API_ID/$proxmox_api_id/" jenkins.yaml
sed -i "s/PLACEHOLDER_PROXMOX_API_KEY/$proxmox_api_key/" jenkins.yaml
sed -i "s/PLACEHOLDER_PROXMOX_USERNAME/$proxmox_username/" jenkins.yaml
sed -i "s/PLACEHOLDER_PROXMOX_PASSWORD/$proxmox_password/" jenkins.yaml
sed -i "s#PLACEHOLDER_WEBHOOK#$discord_webhook#" jenkins.yaml
sed -i "s/PLACEHOLDER_DISCORD_TOKEN/$discord_token/" jenkins.yaml
sed -i "s#PLACEHOLDER_SEED_REPO#$seed_repo#" jenkins.yaml

# Build and run the Docker container
docker build -t jenkins:latest .
docker run -d -p 8080:8080 -p 50000:50000 \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --privileged -v $(which docker):/usr/bin/docker \
  --name jenkins jenkins:latest

echo "Jenkins is starting with the provided admin password at http://localhost:8080."
