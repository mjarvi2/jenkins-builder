import jenkins.security.*
import hudson.model.User
import jenkins.model.JenkinsLocationConfiguration
import com.cloudbees.plugins.credentials.CredentialsScope
import com.cloudbees.plugins.credentials.SystemCredentialsProvider
import com.cloudbees.plugins.credentials.domains.Domain
import org.jenkinsci.plugins.plaincredentials.impl.StringCredentialsImpl

def env = System.getenv()
def userName = 'jenkins'
def tokenName = 'service-token'
jlc = JenkinsLocationConfiguration.get()

def user = User.get(userName, false)
println(user)
if (user != null) {
    def apiTokenProperty = user.getProperty(ApiTokenProperty.class)
    if (!apiTokenProperty.getTokenList().name.contains(tokenName)) {
        def result = apiTokenProperty.tokenStore.generateNewToken(tokenName)
        println(result)
        user.save()
        def tokenValue = result.plainValue
        def credentialsStore = SystemCredentialsProvider.getInstance().getStore()
        def domain = Domain.global()
        def credentialId = 'JENKINS_API_TOKEN'
        def description = 'API token for service'

        def credentials = new StringCredentialsImpl(
            CredentialsScope.GLOBAL,
            credentialId,
            description,
            hudson.util.Secret.fromString(tokenValue)
        )

        credentialsStore.addCredentials(domain, credentials)
        println("Stored API token as Jenkins credential with ID: ${credentialId}")
    }
}
